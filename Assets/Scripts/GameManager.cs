﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{

    [SerializeField]
    private PlayerUI playerUI;

    public static GameManager instance;

    private bool paused;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () 
	{
        LockAndHideCursor(true);
	}
	
	// Update is called once per frame
	void Update () 
	{
        GetInputs();
	}

    void GetInputs()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (paused)
                PauseGame(false);
            else
                PauseGame(true);
        }
    }

    void PauseGame(bool _pause)
    {
        LockAndHideCursor(!_pause);
        paused = _pause;
    }

    void LockAndHideCursor(bool _lockAndHide)
    {
        if (_lockAndHide)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {       
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public bool IsPaused()
    {
        return paused;
    }

    public PlayerUI GetPlayerUI()
    {
        return playerUI;
    }
}
