﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour 
{
    [SerializeField]
    private int maxHp = 10;
    private int curHp;

    private EnemyAnimations eAnim;
    private bool dead;

    void Start()
    {
        //get components
        eAnim = GetComponent<EnemyAnimations>();

        curHp = maxHp;
    }

    public void DamageEnemy(int _damage, Transform _hitBox)
    {
        if (dead)
            return;

        if (_hitBox.tag == "HitBoxCritical")
        {
            curHp = 0;
        }    
        else
            curHp -= _damage;

        if (curHp <= 0)
        {
            curHp = 0;
            Die();
        }

    }

    void Die()
    {
        dead = true;
        eAnim.SetDead(true);
    }

    public int GetCurHp()
    {
        return curHp;
    }

    public bool IsDead()
    {
        return dead;
    }
	
}
