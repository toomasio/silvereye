﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{

    [SerializeField]
    private float playerSpeed = 6;
    [SerializeField]
    private float lookSens = 6;
    [SerializeField]
    private float camRotLimit = 45;
    private float lookX;
    private float lookY;
    [SerializeField]
    private Transform fpsCamera;

    private Vector3 move;
    private Vector2 look;
    private float curCamRot;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (GameManager.instance.IsPaused())
            return;

        GetInputs();
        RotatePlayer();
    }

    void FixedUpdate()
    {
        if (GameManager.instance.IsPaused())
            return;

        MovePlayer();
        
    }

    void GetInputs()
    {
        //movement inputs
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveZ = Input.GetAxisRaw("Vertical");
        move = new Vector3(moveX, 0, moveZ).normalized * Time.deltaTime;

        //rotation inputs
        float mouseX = Input.GetAxisRaw("Mouse X");
        float mouseY = Input.GetAxisRaw("Mouse Y");
        float xboxRX = Input.GetAxisRaw("XboxRStickHor");
        float xboxRY = Input.GetAxisRaw("XboxRStickVer");

        //choose mouse or joystick based on input
        if (mouseX != 0)
            lookX = mouseX;
        else if (xboxRX != 0)
            lookX = xboxRX;
        else
            lookX = 0;

        //choose mouse or joystick based on input
        if (mouseY != 0)
            lookY = mouseY;
        else if (xboxRY != 0)
            lookY = xboxRY;
        else
            lookY = 0;

        //normalize looking to smooth rotational viewing
        if (mouseX ==0 && mouseY == 0)
            look = new Vector2(lookX, lookY).normalized;
        else
            look = new Vector2(lookX, lookY);


    }

    void MovePlayer()
    {
        transform.Translate(move * playerSpeed);
    }

    void RotatePlayer()
    {
        //rotate player
        transform.Rotate(0, look.x * lookSens, 0);

        //clamp camera rotation
        float camRot = look.y * lookSens;
        curCamRot -= camRot;
        curCamRot = Mathf.Clamp(curCamRot, -camRotLimit, camRotLimit);

        //rotate cameraPivot with mouse Y..clamped
        fpsCamera.transform.localEulerAngles = new Vector3(curCamRot, 0f, 0f);
    }
}
