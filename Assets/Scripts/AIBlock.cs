﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBlock:MonoBehaviour
{
    [SerializeField]
    private AIBlock EnterBlock;
    [SerializeField]
    private AIBlock ExitBlock;
    [SerializeField]
    private List<Transform> points = new List<Transform>();

	
	// Update is called once per frame....
	void Update () 
	{
		
	}

    public List<Transform> GetPoints()
    {
        return points;
    }


}
