﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimations : MonoBehaviour 
{

    [SerializeField]
    private Animator anim;

    public void SetSpeed(float _speed)
    {
        anim.SetFloat("speed", _speed);
    }

    public void SetDead(bool _dead)
    {
        anim.SetBool("dead", _dead);
        if (_dead)
        anim.SetInteger("deathNum", Random.Range(0, 3));
    }

    public void SetShoot(bool _shoot)
    {
        anim.SetBool("shoot", _shoot);
    }

    public bool IsCurrentState(string _state)
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName(_state);
    }
}
