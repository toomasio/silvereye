﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{

    [SerializeField]
    private Slider healthBar;
    [Header("Damage Effects")]
    [SerializeField]
    private Image damageImage;
    [SerializeField]
    private float damageFadeTime;
    [Range(0, 1)]
    [SerializeField]
    private float damageStartAlpha = 0.5f;
    private float fadeTimer;

    private float curHealth;

    public void SetMaxHealth(float _maxAmount)
    {
        healthBar.maxValue = _maxAmount;
    }

    public void SetCurHealth(float _curAmount)
    {
        if (_curAmount < curHealth)
        {
            StartCoroutine(DamageFX());
        }

        curHealth = _curAmount;
        healthBar.value = curHealth;
    }

    IEnumerator DamageFX()
    {
        fadeTimer = 0;
        Color col = damageImage.color;
        while (fadeTimer < damageFadeTime)
        {
            fadeTimer += Time.deltaTime;
            float t = fadeTimer / damageFadeTime;

            float alpha = Mathf.Lerp(damageStartAlpha, 0, t);

            col.a = alpha;
            damageImage.color = col;

            yield return new WaitForEndOfFrame();
        }
    }
}
