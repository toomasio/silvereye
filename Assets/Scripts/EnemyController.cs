﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{

    [SerializeField]
    private bool patrol;
    [SerializeField]
    private float patrolSpeed = 0.75f;
    [SerializeField]
    private List<Transform> points = new List<Transform>(); //using list so we can reverse sort for ping pong
    [SerializeField]
    private float arrivalStopTime = 1;
    private float stopTimer;
    [SerializeField]
    private bool pingPong;
    [SerializeField]
    private float runSpeed = 3;
    [SerializeField]
    private bool chasePlayer;
    private bool chasing;
    [SerializeField]
    private float detectIntervals = 1;
    private float detectTimer;
    [SerializeField]
    private float chaseDistance = 20;

    private Vector3 curPos;
    private Vector3 smoothPos;
    private int curInd = 0;

    private float curDustance;

    private EnemyShoot es;
    private EnemyAnimations eAnim;
    private Enemy enemy;
    private Vector3 last;
    private float curSpeed;

    private NavMeshAgent agent;

    private Vector3 lastPos;
    private Transform player;

    // Use this for initialization
    void Start()
    {
        //get components
        es = GetComponent<EnemyShoot>();
        eAnim = GetComponent<EnemyAnimations>();
        agent = GetComponent<NavMeshAgent>();
        enemy = GetComponent<Enemy>();

        if (patrol)
        {
            agent.speed = patrolSpeed;
            curInd = -1;
            NextWaypoint();         
        }

        StartCoroutine(CalcSpeed());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (enemy.IsDead())
        {
            if (curSpeed > 0)
                agent.Stop();

            return;
        }
            

        //upate animations
        eAnim.SetSpeed(curSpeed);

        MoveEnemy();
    }

    void MoveEnemy()
    {
        if (chasing)
        {
            ChasePlayer();
        }
        else if (patrol)
        {
            Patrol();
        }
             
    }

    void Patrol()
    {
        //check if agent has arrived at destination
        if (!agent.pathPending)//is not computing path?
        {
            //make sure remaining distance is less or equal to stopping distance
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                //check is agent has no path and is stopped
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                {
                    
                        //switch to next waypoint
                        NextWaypoint();                
                }
            }
        }
    }

    void NextWaypoint()
    {
        stopTimer += Time.deltaTime;
        if (stopTimer > arrivalStopTime)//wait at point
        {
            if (curInd < points.Count - 1)
                curInd++;
            else
            {
                if (pingPong)
                {
                    points.Reverse();
                    curInd = 1;
                }else
                    curInd = 0;

            }

            curPos = points[curInd].position;
            agent.SetDestination(curPos);
            stopTimer = 0;
        }

    }

    public void PausePatrol(bool _pause)
    {
        if (_pause)
            agent.Stop();
        else
        {
            agent.Resume();
            agent.speed = patrolSpeed;
            agent.SetDestination(curPos);
        }
            
    }

    public void StartChasePlayer(Transform _player)
    {
        chasing = true;
        player = _player;
        agent.Resume();
        agent.speed = runSpeed;        
    }

    void ChasePlayer()
    {
        detectTimer += Time.deltaTime;
        if (detectTimer > detectIntervals)
        {
            agent.SetDestination(player.position);
            detectTimer = 0;
        }

        float distance = Vector3.Distance(transform.position, player.position);
        if (distance > chaseDistance)
        {
            chasing = false;
            PausePatrol(false);
        }
    }

    public bool IsChase()
    {
        return chasePlayer;
    }

    IEnumerator CalcSpeed()
    {
        while (Application.isPlaying)
        {
            // Position at frame start
            lastPos = transform.position;
            // Wait till it the end of the frame
            yield return new WaitForEndOfFrame();
            // Calculate velocity: Velocity = DeltaPosition / DeltaTime
            curSpeed = ((lastPos - transform.position) / Time.deltaTime).magnitude;
        }
    }

}