﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour 
{

    [SerializeField]
    private int maxHp = 5;
    [SerializeField]
    private int maxLives = 3;
    [SerializeField]
    private Transform enemyTarget;

    private int curHp;
    private int curLives;
    private PlayerUI plUI;

    // Use this for initialization
    void Start()
    {
        //get components
        plUI = GameManager.instance.GetPlayerUI();

        curLives = maxLives;
        SetDefaults();
    }

    void SetDefaults()
    {
        curHp = maxHp;

        //update UI
        plUI.SetMaxHealth(maxHp);
        plUI.SetCurHealth(curHp);
    }

    public void AddHp(int _amount)
    {
        //only add health if not at max
        if (curHp < maxHp)
        {
            curHp += _amount;
        }

        //update UI
        plUI.SetCurHealth(curHp);

    }

    public void DamageHp(int _damage)
    {

        curHp -= _damage;

        //kill player if health at 0
        if (curHp <= 0)
        {
            curHp = 0;
            Die();
        }

        //update UI
        plUI.SetCurHealth(curHp);
    }

    void Die()
    {

    }

    public int GetCurHp()
    {
        return curHp;
    }

    public Transform GetEnemyTarget()
    {
        return enemyTarget;
    }
}
