﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour 
{

    [SerializeField]
    private int damage;
    [SerializeField]
    private float fov = 45;
    [SerializeField]
    private float rotSpeed = 50;
    [SerializeField]
    private float shootDelay = 0.2f;
    private float shootTimer;
    [SerializeField]
    private float reloadTime = 2;
    private float reloadTimer;
    [SerializeField]
    private int clipSize = 30;
    private int curAmmo;
    [SerializeField]
    private float spread = 1;
    [SerializeField]
    private Transform nozzle;
    [SerializeField]
    private LayerMask hitMask;


    private Transform target;
    private bool attack;
    private bool reloading;
    private EnemyController ec;
    private EnemyAnimations eAnim;
    private Enemy enemy;

    private bool shoot;

	// Use this for initialization
	void Start () 
	{
        //get components
        ec = GetComponent<EnemyController>();
        eAnim = GetComponent<EnemyAnimations>();
        enemy = GetComponent<Enemy>();

        curAmmo = clipSize;
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (enemy.IsDead())
            return;

        if (attack)
        {
            if (!shoot)
            {
                shoot = true;
                eAnim.SetShoot(shoot);//update anim
            }

            LookAtPlayer();
            AttackPlayer();
        }
    }

    void AttackPlayer()
    {
        //only fire weapon after delay
        shootTimer += Time.deltaTime;
        if (shootTimer > shootDelay)
        {
            FireWeapon();
            shootTimer = 0; //reset timer
        }

        if (curAmmo <= 0) //reload if no ammo
        {
            ReloadWeapon();
        }
    }

    void HitFX(Vector3 _pos, Vector3 _normal)
    {
        Quaternion rot = Quaternion.LookRotation(_normal);
        //Instantiate(hitFX, _pos, rot);
    }

    void FireWeapon()
    {
        if (reloading || !eAnim.IsCurrentState("Shoot"))
            return;

        //random accuracy vector based off of spread
        float randX = Random.Range(target.position.x - spread, target.position.x + spread);
        float randY = Random.Range(target.position.y - spread, target.position.y + spread);
        float randZ = Random.Range(target.position.z - spread, target.position.z + spread);
        Vector3 lookPos = new Vector3(randX, randY, randZ);

        //nozzle looks at accuracy position
        nozzle.LookAt(lookPos);

        //ray forward from nozzle
        Ray ray = new Ray(nozzle.position, nozzle.forward);

        //shoot ray
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, hitMask))
        {
            //spawn fx
            HitFX(hitInfo.point, hitInfo.normal);

            //damage player
            if (hitInfo.collider.tag == "Player")
            {
                hitInfo.collider.GetComponent<Player>().DamageHp(damage);
            }
        }
        //debug ray
        Debug.DrawRay(ray.origin, ray.direction * 20, Color.red);

        //minus ammo
        curAmmo--;
    }

    void ReloadWeapon()
    {
        reloading = true;
        reloadTimer += Time.deltaTime;
        if (reloadTimer > reloadTime)
        {
            reloading = false;
            curAmmo = clipSize; //replenish ammo
            reloadTimer = 0; //reset timer
        }
    }

    void LookAtPlayer()
    {
        //lookat direction...ignores y axis so enemy doesnt tilt up
        Vector3 dir = new Vector3(target.position.x, transform.position.y, target.position.z) - transform.position;
        //Look at and dampen the rotation
        Quaternion rot = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, rotSpeed * Time.deltaTime);
    }

    public void PlayerInRange(Transform _target)
    {
        target = _target;

        //attack only if facing player
        if (Vector3.Angle(transform.forward, target.position - transform.position) < fov 
            && !attack)
        {
            attack = true;
            ec.PausePatrol(true);
        }
    }

    public void StopAttacking(bool _stop)
    {
        if (_stop)
        {
            //chase player if enabled and out of range.
            if (ec.IsChase() && attack)
            {
                ec.StartChasePlayer(target.root);
            }

            attack = false;

            if (shoot)
            {
                shoot = false;
                eAnim.SetShoot(shoot);//update anim
            }
        }
    }

    public bool IsAttacking()
    {
        return attack;
    }
}
