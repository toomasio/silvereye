﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour 
{

    [SerializeField]
    private int damage = 30;
    [SerializeField]
    private Transform nozzle;
    [SerializeField]
    private LayerMask hitMask;
    [SerializeField]
    private GameObject hitFX;
    [SerializeField]
    private Camera plCamera;


    private bool fire;
    private Vector3 center;

	// Use this for initialization
	void Start () 
	{
        center = new Vector3(0.5f,0.5f,0);
	}
	
	// Update is called once per frame
	void Update () 
	{
        GetInputs();
	}

    void GetInputs()
    {
        fire = Input.GetButtonDown("Fire1");

        if (fire)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        //shoot ray from camera
        Ray screenRay = plCamera.ViewportPointToRay(center);
        RaycastHit screenHit;
        if (Physics.Raycast(screenRay, out screenHit, Mathf.Infinity, hitMask))
        {
            //nozzle looks at target
            nozzle.LookAt(screenHit.point);
            //initialize ray from nozzle
            Ray ray = new Ray(nozzle.position, nozzle.forward);
            //initialize raycast hit info
            RaycastHit hitInfo;
            //fire raycast from nozzle
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, hitMask))
            {
                //spawn fx on hit
                HitFX(hitInfo.point, hitInfo.normal);

                //damage enemy if hitbox
                if (hitInfo.collider.gameObject.layer == LayerMask.NameToLayer("HitBox"))
                {
                    //get transform from hit collider
                    Transform hitBox = hitInfo.collider.transform;
                    //get enemy script from the ROOT of the hitbox (main parent..ie the controller)
                    Enemy enemy = hitBox.root.GetComponent<Enemy>();
                    //damage enemy
                    DamageEnemy(enemy, hitBox);

                    //get enemy to chase player when shot
                    EnemyController ec = enemy.GetComponent<EnemyController>();
                    ec.StartChasePlayer(transform);
                }
            }
        }

        
    }

    void DamageEnemy(Enemy _enemy, Transform _hitbox)
    {
        _enemy.DamageEnemy(damage, _hitbox);
    }

    void HitFX(Vector3 _pos, Vector3 _normal)
    {
        Quaternion rot = Quaternion.LookRotation(_normal);
        Instantiate(hitFX, _pos, rot);
    }
}
