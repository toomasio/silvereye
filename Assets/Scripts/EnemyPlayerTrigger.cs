﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPlayerTrigger : MonoBehaviour 
{

    [SerializeField]
    private EnemyShoot enemyShoot;

	void OnTriggerStay(Collider _col)
    {
        if (_col.tag == "Player")
            //get target from player script
            enemyShoot.PlayerInRange(_col.GetComponent<Player>().GetEnemyTarget());
    }

    void OnTriggerExit(Collider _col)
    {
        if (_col.tag == "Player")
            enemyShoot.StopAttacking(true);
    }
}
